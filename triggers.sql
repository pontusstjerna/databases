CREATE VIEW limitationHelper AS
SELECT COUNT(nationalid), registeredto.course, limitedcourse.totalenrolledamount 
FROM limitedcourse JOIN registeredto 
    ON limitedcourse.code = registeredto.course 
GROUP BY course,totalenrolledamount;


CREATE FUNCTION putIntoQueue() RETURNS TRIGGER AS $$
    BEGIN
        IF NEW.studentname = NULL THEN
            NEW.studentname := (SELECT name FROM student WHERE nationalid = NEW.nationalid);
        END IF;
        IF NEW.coursename = NULL THEN
            NEW.coursename := (SELECT name FROM course WHERE code = NEW.coursecode);
        END IF;
        
        NEW.status = 'Registered';
        
        IF EXISTS (SELECT * FROM requires WHERE coursecode = NEW.coursecode) THEN
            IF NOT(select count(code) from passedcourses join requires on passedcourses.code = requires.reqcourse WHERE (nationalid,coursecode) = (NEW.nationalid,NEW.coursecode)) = (select count(reqcourse) FROM requires GROUP BY coursecode HAVING coursecode = NEW.coursecode) THEN
                RAISE EXCEPTION 'STUDENT NOT FULLFILLED REQUIREMENTS'
                USING HINT = 'Please read the required courses.';
                RETURN NULL;
            END IF;
        END IF;
      IF NOT EXISTS (SELECT nationalid,coursecode FROM Registrations WHERE (nationalid, coursecode) = (NEW.nationalid, NEW.coursecode)) THEN
        IF EXISTS (SELECT code FROM limitedcourse WHERE code = NEW.coursecode) THEN
                IF (SELECT count FROM limitationhelper WHERE limitationhelper.course = NEW.coursecode) >= (SELECT totalenrolledamount FROM limitationhelper WHERE limitationhelper.course = NEW.coursecode ) THEN
                    INSERT INTO waitsfor(nationalid, code, queueposition) VALUES (NEW.nationalid, NEW.coursecode, (SELECT (coalesce(queuepos.currentPos, 0)) + 1 FROM (SELECT MAX(queueposition) AS currentPos FROM waitsfor WHERE waitsfor.code = NEW.coursecode) AS queuepos));
                ELSE
                   INSERT INTO registeredto(course, nationalid) VALUES (NEW.coursecode, NEW.nationalid);
                END IF;
        ELSE
            INSERT INTO registeredto(course, nationalid) VALUES (NEW.coursecode, NEW.nationalid);
        END IF;
        
       ELSE
        RAISE EXCEPTION 'STUDENT ALREADY REGISTERED OR IN QUEUE'
        USING HINT = 'Please be more patient.';
       END IF;
        RETURN NULL;
    END
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER coursefull 
INSTEAD OF INSERT ON Registrations
FOR EACH ROW
EXECUTE PROCEDURE putIntoQueue();


CREATE FUNCTION removeFromQueue() RETURNS TRIGGER AS $$
    BEGIN
        IF OLD.status = 'Registered' THEN
        DELETE FROM registeredto WHERE (nationalid,course) = (OLD.nationalid, OLD.coursecode);
            IF EXISTS (SELECT queueposition FROM waitsfor WHERE code = OLD.coursecode) THEN
                WITH firstInQueue AS (DELETE FROM waitsfor WHERE (queueposition,code) = (1,OLD.coursecode) RETURNING nationalid, code)
                INSERT INTO registeredto (nationalid, course) SELECT * FROM firstInQueue;
            END IF;
        ELSE
            DELETE FROM waitsfor WHERE (nationalid, course) = (OLD.nationalid, OLD.coursecode);
            UPDATE waitsfor SET queueposition = waitsfor.queueposition - 1 WHERE queueposition > 1;
        END IF;
    RETURN NULL;
    END
$$ LANGUAGE 'plpgsql';


CREATE TRIGGER registrationAvailable
INSTEAD OF DELETE ON Registrations
FOR EACH ROW
EXECUTE PROCEDURE removeFromQueue();