INSERT INTO department VALUES('Computer Science and Engineering', 'CSE');
INSERT INTO department VALUES('Nature Science and Abusement', 'NSA');

INSERT INTO programme VALUES('Computer Science', 'CS');
INSERT INTO programme VALUES('Fishing', 'FSK');
INSERT INTO programme VALUES('Hunting', 'HNT');

INSERT INTO hosts VALUES('Computer Science', 'Computer Science and Engineering');
INSERT INTO hosts VALUES('Fishing', 'Nature Science and Abusement');
INSERT INTO hosts VALUES('Hunting', 'Nature Science and Abusement');


INSERT INTO course VALUES('Databases', 'DIT620', 7, 'Computer Science and Engineering');
INSERT INTO course VALUES('Algorithms', 'DIT010', 7, 'Computer Science and Engineering');
INSERT INTO course VALUES('Coding', 'DIT555', 7, 'Computer Science and Engineering');
INSERT INTO course VALUES('Computer Rooms', 'DIT512', 7, 'Computer Science and Engineering');
INSERT INTO course VALUES('Infinite Automata', 'DIT123', 8, 'Computer Science and Engineering');
INSERT INTO course VALUES('Anger Management', 'DIT666', 666, 'Computer Science and Engineering');
INSERT INTO course VALUES('Bait Application', 'BAI136', 1, 'Nature Science and Abusement');
INSERT INTO course VALUES('Fish Simulation', 'BAI344', 1, 'Nature Science and Abusement');
INSERT INTO course VALUES('Arctic Fishing', 'BAI500', 1, 'Nature Science and Abusement');
INSERT INTO course VALUES('Forest Running', 'FOR500', 2, 'Nature Science and Abusement');
INSERT INTO course VALUES('Tree hugging', 'FOR501', 2, 'Nature Science and Abusement');
INSERT INTO course VALUES('Deer Anatomy', 'HUN120', 3, 'Nature Science and Abusement');
INSERT INTO course VALUES('Rabbit Slaughter', 'HUN121', 3, 'Nature Science and Abusement');
INSERT INTO course VALUES('Rifle Handling', 'HUN122', 3, 'Nature Science and Abusement');
INSERT INTO course VALUES('Fletching', 'HUN123', 3, 'Nature Science and Abusement');
INSERT INTO course VALUES('Trumping', 'POL53', 4, 'Computer Science and Engineering');
INSERT INTO course VALUES('Not Trumping', 'POL54', 4, 'Computer Science and Engineering');
INSERT INTO course VALUES('Analytics', 'POL51', 4, 'Computer Science and Engineering');

INSERT INTO mandatory VALUES('Computer Science', 'DIT629');
INSERT INTO mandatory VALUES('Computer Science', 'DIT010');
INSERT INTO mandatory VALUES('Computer Science', 'DIT555');
INSERT INTO mandatory VALUES('Computer Science', 'DIT512');
INSERT INTO mandatory VALUES('Computer Science', 'DIT123');
INSERT INTO mandatory VALUES('Computer Science', 'DIT666');
INSERT INTO mandatory VALUES('Fishing', 'BAI136');
INSERT INTO mandatory VALUES('Fishing', 'BAI344');
INSERT INTO mandatory VALUES('Fishing', 'BAI500');
INSERT INTO mandatory VALUES('Hunting', 'HUN120');
INSERT INTO mandatory VALUES('Hunting', 'HUN121');
INSERT INTO mandatory VALUES('Hunting', 'HUN122');
INSERT INTO mandatory VALUES('Hunting', 'HUN123');


INSERT INTO limitedcourse VALUES('POL53', 2);
INSERT INTO limitedcourse VALUES('POL54', 1);
INSERT INTO limitedcourse VALUES('BAI344', 50);

INSERT INTO holds VALUES('Fishing', 'BAI136');
INSERT INTO holds VALUES('Fishing', 'BAI344');
INSERT INTO holds VALUES('Fishing', 'BAI500');
INSERT INTO holds VALUES('Computer Science', 'DIT620');
INSERT INTO holds VALUES('Computer Science', 'DIT010');
INSERT INTO holds VALUES('Computer Science', 'DIT555');
INSERT INTO holds VALUES('Computer Science', 'DIT512');
INSERT INTO holds VALUES('Computer Science', 'DIT123');
INSERT INTO holds VALUES('Computer Science', 'DIT666');
INSERT INTO holds VALUES('Hunting', 'HUN120');
INSERT INTO holds VALUES('Hunting', 'HUN121');
INSERT INTO holds VALUES('Hunting', 'HUN122');
INSERT INTO holds VALUES('Hunting', 'HUN123');

INSERT INTO student VALUES (5, 'Magnus Efternamn', 'Computer Science');
INSERT INTO student VALUES (150, 'Clark Kent', 'Computer Science');
INSERT INTO student VALUES (950630, 'Pontus Stjernstrom', 'Computer Science');
INSERT INTO student VALUES (950000, 'Aleksandar Babunovic', 'Hunting');
INSERT INTO student VALUES (151, 'Bruce  Willis', 'Hunting');
INSERT INTO student VALUES (0, 'Nat Pagle', 'Fishing');
INSERT INTO student VALUES (18001, 'Fredik Reinfeldt', 'Fishing');
INSERT INTO student VALUES (1, 'Hemet Nesingwary', 'Hunting');

INSERT INTO branch VALUES ('Hunting', 'Godzilla Hunting');
INSERT INTO branch VALUES ('Fishing', 'Sushi Fishing');
INSERT INTO branch VALUES ('Computer Science', 'AI');
INSERT INTO branch VALUES ('Computer Science', 'Security');
INSERT INTO branch VALUES ('Hunting', 'Simulation');

INSERT INTO classification VALUES ('Mathematical');
INSERT INTO classification VALUES ('Research');
INSERT INTO classification VALUES ('Seminar');

INSERT INTO registeredTo VALUES (0, 'BAI344');
INSERT INTO registeredTo VALUES (1, 'HUN123');
INSERT INTO registeredTo VALUES (1, 'HUN122');
INSERT INTO registeredTo VALUES (1, 'HUN121');
INSERT INTO registeredTo VALUES (18001, 'POL53');
INSERT INTO registeredTo VALUES (950630, 'DIT666');
INSERT INTO registeredto VALUES('950000', 'POL53');

INSERT INTO hasFinished VALUES (0, 'BAI500','5');
INSERT INTO hasFinished VALUES (0, 'BAI136', '5');
INSERT INTO hasFinished VALUES (950000, 'HUN121','U');
INSERT INTO hasfinished values (950630, 'DIT555', '3');
INSERT INTO hasfinished values (950630, 'DIT666', '5');
INSERT INTO hasfinished values (950630, 'DIT010', '4');
INSERT INTO hasfinished values (5, 'DIT010', '4');

INSERT INTO demands VALUES ('Hunting', 'Godzilla Hunting', 'HUN121');
INSERT INTO demands VALUES ('Fishing', 'Sushi Fishing', 'BAI136');
INSERT INTO demands VALUES ('Computer Science', 'Security', 'POL54');

INSERT INTO recommends VALUES ('DIT123', 'Fishing', 'Sushi Fishing');
INSERT INTO recommends VALUES ('DIT555', 'Hunting', 'Godzilla Hunting');

INSERT INTO reads VALUES (950000, 'Godzilla Hunting', 'Hunting');
INSERT INTO reads VALUES (5, 'Security', 'Computer Science');

INSERT INTO has VALUES ('DIT555', 'Mathematical');
INSERT INTO has VALUES ('DIT010', 'Mathematical');
INSERT INTO has VALUES ('DIT666', 'Mathematical');
INSERT INTO has VALUES ('DIT666', 'Research');
INSERT INTO has VALUES ('DIT123', 'Research');
INSERT INTO has VALUES ('DIT555', 'Seminar');
