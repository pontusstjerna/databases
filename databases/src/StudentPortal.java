/* This is the driving engine of the program. It parses the command-line
 * arguments and calls the appropriate methods in the other classes.
 *
 * You should edit this file in two ways:
 * 1) Insert your database username and password in the proper places.
 * 2) Implement the three functions getInformation, registerStudent
 *    and unregisterStudent.
 */
import org.postgresql.util.PSQLException;

import java.sql.*; // JDBC stuff.
import java.util.Properties;
import java.io.*;  // Reading user input.
import java.util.*;

public class StudentPortal
{
    /* TODO Here you should put your database name, username and password */
    //static final String USERNAME = "tda357_043";
    //static final String PASSWORD = "LnWVkGtm";
    static final String USERNAME = "pontu";
    static final String PASSWORD = "kaka1";

    /* Print command usage.
     * /!\ you don't need to change this function! */
    public static void usage () {
        System.out.println("Usage:");
        System.out.println("    i[nformation]");
        System.out.println("    r[egister] <course>");
        System.out.println("    u[nregister] <course>");
        System.out.println("    q[uit]");
    }

    /* main: parses the input commands.
     * /!\ You don't need to change this function! */
    public static void main(String[] args) throws Exception
    {
        try {
            Class.forName("org.postgresql.Driver");
            //String url = "jdbc:postgresql://ate.ita.chalmers.se/";
            String url = "jdbc:postgresql://localhost/lokalsak";
            Properties props = new Properties();
            props.setProperty("user",USERNAME);
            props.setProperty("password",PASSWORD);
            Connection conn = DriverManager.getConnection(url, props);

            String student = args[0]; // This is the identifier for the student

        //    Console console = System.console();
	    // In Eclipse. System.console() returns null due to a bug (https://bugs.eclipse.org/bugs/show_bug.cgi?id=122429)
	    // In that case, use the following line instead:
	     BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
            usage();
            System.out.println("Welcome!");
            while(true) {
	        System.out.print("? > ");
                String mode = console.readLine();
                String[] cmd = mode.split(" +");
                cmd[0] = cmd[0].toLowerCase();
                if ("information".startsWith(cmd[0]) && cmd.length == 1) {
                    /* Information mode */
                    getInformation(conn, student);
                } else if ("register".startsWith(cmd[0]) && cmd.length == 2) {
                    /* Register student mode */
                    registerStudent(conn, student, cmd[1]);
                } else if ("unregister".startsWith(cmd[0]) && cmd.length == 2) {
                    /* Unregister student mode */
                    unregisterStudent(conn, student, cmd[1]);
                } else if ("quit".startsWith(cmd[0])) {
                    break;
                } else usage();
            }
            System.out.println("Goodbye!");
            conn.close();
        } catch (SQLException e) {
            System.err.println(e);
            System.exit(2);
        }
    }

    /* Given a student identification number, ths function should print
     * - the name of the student, the students national identification number
     *   and their issued login name (something similar to a CID)
     * - the programme and branch (if any) that the student is following.
     * - the courses that the student has read, along with the grade.
     * - the courses that the student is registered to. (queue position if the student is waiting for the course)
     * - the number of mandatory courses that the student has yet to read.
     * - whether or not the student fulfills the requirements for graduation
     */
    static void getInformation(Connection conn, String student) throws SQLException
    {

        //This is the ugliest code ever written
        PreparedStatement statement = conn.prepareStatement("SELECT nationalId, " +
                                                               "name, programme, " +
                                                               "totalcredits, researchCredits, " +
                                                               "mathcredits, numberofseminarspassed," +
                                                               " cangraduate FROM pathToGraduation WHERE nationalId = ?");


        statement.setInt(1,Integer.parseInt(student));
        ResultSet resultsetPathToGrad = statement.executeQuery();

        statement = conn.prepareStatement("SELECT branch FROM reads WHERE nationalid = ?");
        statement.setInt(1,Integer.parseInt(student));

        ResultSet resultsetBranchName = statement.executeQuery();

        statement = conn.prepareStatement("SELECT code,grade,name,credit FROM hasFinished NATURAL JOIN course WHERE nationalid = ?");
        statement.setInt(1,Integer.parseInt(student));

        ResultSet resultFinishedCourses = statement.executeQuery();

        statement = conn.prepareStatement("SELECT coursecode, status, queueposition, coursename FROM Registrations " +
                "LEFT OUTER JOIN waitsfor ON (registrations.nationalid, registrations.coursecode) = " +
                "(waitsfor.nationalid, waitsfor.code) WHERE registrations.nationalid = ?");
        statement.setInt(1,Integer.parseInt(student));

        ResultSet resultRegisteredCourses = statement.executeQuery();

        resultsetPathToGrad.next();

        System.out.println("Information for student " + student);
        System.out.println("----------------------------------------");
        System.out.println("Name: " + resultsetPathToGrad.getString(2));
        System.out.println("Student ID: " +
                resultsetPathToGrad.getString(2).substring(0,2).toLowerCase() +
                resultsetPathToGrad.getString(2).substring(resultsetPathToGrad.getString(2).lastIndexOf(" ") +
                        1,resultsetPathToGrad.getString(2).lastIndexOf(" ") + 3).toLowerCase());
        System.out.println("Line: " + resultsetPathToGrad.getString(3));
        if(resultsetBranchName.next()) {
            System.out.println("Branch: " + resultsetBranchName.getString(1) + "\n");
        }
        else
        {
            System.out.println("Branch: none\n");
        }
        System.out.println("Read courses (name (code), credits: grade): ");

        while(resultFinishedCourses.next())
        {
            System.out.println(" " + resultFinishedCourses.getString(3) +
                    " (" + resultFinishedCourses.getString(1) + "), " +
                    resultFinishedCourses.getString(4) + "p: " +
                    resultFinishedCourses.getString(2));
        }


        System.out.println("\nRegistered courses (name (code): status): ");

        while(resultRegisteredCourses.next())
        {
            System.out.print(" " + resultRegisteredCourses.getString(4) +
                    " (" + resultRegisteredCourses.getString(1) +
                    "): " + resultRegisteredCourses.getString(2));
            if(resultRegisteredCourses.getString(2).equals("Waiting")) {
                System.out.println(" as nr " + resultRegisteredCourses.getInt(3));
            }else
                System.out.println();
        }
        System.out.println("\nSeminar courses passed: " +resultsetPathToGrad.getInt(7));
        System.out.println("Math credits taken: " + resultsetPathToGrad.getInt(6));
        System.out.println("Research Credits: " +resultsetPathToGrad.getInt(5));
        System.out.println("Total credits: " +resultsetPathToGrad.getInt(4));
        System.out.println("Fulfills the requirements for graduation: " + resultsetPathToGrad.getString(8));
        System.out.println("-------------------------------------------------------------------");


        statement.close();
        resultsetPathToGrad.close();
        resultFinishedCourses.close();
        resultRegisteredCourses.close();
        resultsetBranchName.close();

    }

    /* Register: Given a student id number and a course code, this function
     * should try to register the student for that course.
     */
    static void registerStudent(Connection conn, String student, String course)
    throws SQLException
    {
        PreparedStatement statement = conn.prepareStatement("INSERT INTO registrations VALUES(?, NULL, ?, NULL, NULL)");

        statement.setInt(1, Integer.parseInt(student));
        statement.setString(2,course);
        try{
            statement.executeQuery();
        }catch (PSQLException exception){
            String ex = exception.toString();
            if(ex.contains("ERROR"))
                System.out.println(exception.toString());
            else
                System.out.println(student + " successfully registered to " + course + ".");
        }
        statement.close();
    }

    /* Unregister: Given a student id number and a course code, this function
     * should unregister the student from that course.
     */
    static void unregisterStudent(Connection conn, String student, String course)
            throws SQLException {
        String msg = " failed to register to ";
        String status = "blab";

        PreparedStatement statement = conn.prepareStatement("SELECT status FROM registrations WHERE nationalid = ?");
        statement.setInt(1, Integer.parseInt(student));
        ResultSet resStatus = statement.executeQuery();
        if(resStatus.next())
            status = resStatus.getString(1);
        if(status.equals("Waiting"))
        {
            statement = conn.prepareStatement(" DELETE FROM waitsfor WHERE nationalid = ? AND code = ?");

            statement.setInt(1, Integer.parseInt(student));
            statement.setString(2, course);

            try {
                int rtn =  statement.executeUpdate();
                if(rtn == 0)
                    msg = " not even in queue to ";
                else
                    msg = " successfully removed from queue to ";
            } catch (PSQLException exception) {
                String ex = exception.toString();
                if (!ex.contains("return"))
                    System.out.println(exception.toString());
            }
        }else if(status.equals("blab")){
            statement = conn.prepareStatement(" DELETE FROM registrations WHERE nationalid = ? AND course = ?");

            statement.setInt(1, Integer.parseInt(student));
            statement.setString(2, course);

            try {
                int rtn =  statement.executeUpdate();
                if(rtn == 0)
                    msg = " already unregistered to ";
                else
                    msg = " successfully unregistered to ";
            } catch (PSQLException exception) {
                String ex = exception.toString();
                if (!ex.contains("return"))
                    System.out.println(exception.toString());
            }
        }

        System.out.println(student + msg + course + ".");
        statement.close();
    }
}
