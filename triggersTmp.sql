-- CREATE VIEW limitationHelper AS
-- SELECT COUNT(nationalid), registeredto.course, limitedcourse.totalenrolledamount 
-- FROM limitedcourse JOIN registeredto 
    -- ON limitedcourse.code = registeredto.course 
-- GROUP BY course,totalenrolledamount;


-- CREATE FUNCTION putIntoQueue() RETURNS TRIGGER AS $$
    -- BEGIN
      -- IF NOT EXISTS (SELECT nationalid,course FROM Registrations WHERE (nationalid, course) = (NEW.nationalid, NEW.course)) THEN
        -- IF EXISTS (SELECT code FROM limitedcourse WHERE code = NEW.course) THEN
				    -- IF (SELECT count FROM limitationhelper WHERE limitationhelper.course = NEW.course) >= (SELECT totalenrolledamount FROM limitationhelper WHERE limitationhelper.course = NEW.course ) THEN
				        -- INSERT INTO waitsfor(nationalid, code, queueposition) VALUES (NEW.nationalid, NEW.course, (SELECT (coalesce(queuepos.currentPos, 0)) + 1 FROM (SELECT MAX(queueposition) AS currentPos FROM waitsfor WHERE waitsfor.code = NEW.course) AS queuepos));
				    -- END IF;
            -- END IF;
       -- ELSE
        -- RAISE EXCEPTION 'STUDENT ALREADY REGISTERED OR IN QUEUE'
        -- USING HINT 'Please be more patient.';
       -- END IF;
        -- RETURN NULL;
    -- END
-- $$ LANGUAGE 'plpgsql';

-- CREATE TRIGGER coursefull 
-- INSTEAD OF UPDATE ON Registrations
-- FOR EACH ROW
-- EXECUTE PROCEDURE putIntoQueue();














CREATE VIEW limitationHelper AS
SELECT COUNT(nationalid), registeredto.course, limitedcourse.totalenrolledamount 
FROM limitedcourse JOIN registeredto 
    ON limitedcourse.code = registeredto.course 
GROUP BY course,totalenrolledamount;


CREATE FUNCTION putIntoQueue() RETURNS TRIGGER AS $$
    BEGIN
        IF EXISTS (SELECT code FROM limitedcourse WHERE code = NEW.course) THEN
                IF (SELECT count FROM limitationhelper WHERE limitationhelper.course = NEW.course) >= (SELECT totalenrolledamount FROM limitationhelper WHERE limitationhelper.course = NEW.course ) THEN
                    BEGIN
                        INSERT INTO waitsfor(nationalid, code, queueposition) VALUES (NEW.nationalid, NEW.course, (SELECT (coalesce(queuepos.currentPos, 0)) + 1 FROM (SELECT MAX(queueposition) AS currentPos FROM waitsfor WHERE waitsfor.code = NEW.course) AS queuepos));
                        EXCEPTION WHEN unique_violation THEN
                                RAISE EXCEPTION 'STUDENT ALREADY IN QUEUE'
                                USING HINT = 'Please just be patient.';
                        END;
                    DELETE FROM registeredto WHERE (nationalid,course) = (NEW.nationalid,NEW.course);   
                END IF;
        END IF;
    RETURN NULL;
    END
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER coursefull 
AFTER INSERT ON registeredto
FOR EACH ROW
EXECUTE PROCEDURE putIntoQueue();

