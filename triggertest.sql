--************************
--******TRIGGER TESTS*****
--************************

SELECT * FROM registrations;

-- Should fail
INSERT INTO registeredto VALUES(950000,'POL53');

-- Should be but into queue with positions 1,2,3,4
INSERT INTO registeredto VALUES(950630,'POL53');
INSERT INTO registeredto VALUES(5,'POL53');
INSERT INTO registeredto VALUES(0,'POL53');
INSERT INTO registeredto VALUES(1,'POL53');

SELECT * FROM registrations;
SELECT * FROM waitsfor;

-- Should fail because he is already in queue
INSERT INTO registeredto VALUES(950630,'POL53');

-- Now, 950630 should be registered for POL53 and 
-- all other students in queue should be moved one step ahead
DELETE FROM registeredto WHERE nationalid = 950000 AND course = 'POL53';

SELECT * FROM registrations;
SELECT * FROM waitsfor;

-- The case if an admin deletes someone in the queue, 
-- the others should move one step ahead
DELETE FROM waitsfor WHERE nationalid = 5 AND code = 'POL53';

SELECT * FROM waitsfor;