
CREATE VIEW StudentsFollowing 
	AS SELECT student.nationalid, student.name, student.programme, reads.branch 
    FROM student 
    LEFT OUTER JOIN reads 
        ON student.nationalid = reads.nationalid;

CREATE VIEW FinishedCourses 
	AS SELECT *
	FROM hasFinished 
	NATURAL JOIN course 
	NATURAL JOIN 
		(SELECT nationalId, name AS studentName, programme from student) AS temp;
        
CREATE VIEW PassedCourses 
    AS SELECT * 
    FROM hasfinished 
    NATURAL JOIN course 
    NATURAL JOIN 
        (SELECT nationalid, name AS studentName, programme FROM student) AS temp 
    WHERE grade != 'U';

CREATE VIEW UnreadMandatory
AS 
(select nationalId, coursecode from reads   join demands   ON reads.branch = demands.branchname WHERE (demands.coursecode, reads.nationalid)  NOT IN (select code, nationalid from passedcourses))
UNION
(select nationalId, coursecode from student join mandatory ON student.programme = mandatory.programmename WHERE (mandatory.coursecode, student.nationalid)  NOT IN (select code, nationalid from passedcourses)) ORDER BY nationalid;


CREATE VIEW Registrations AS
(SELECT student.nationalid, student.name AS StudentName, course.code AS CourseCode, course.name AS CourseName, 'Waiting' AS Status 
FROM waitsfor, student, course 
WHERE student.nationalid = waitsfor.nationalid 
AND waitsfor.code = course.code) UNION (SELECT student.nationalid AS StudentId, student.name AS StudentName, course.code AS CourseCode, course.name AS CourseName, 'Registered' AS Status 
FROM registeredto, student, course WHERE student.nationalid = registeredto.nationalid AND registeredto.course = course.code);

--SELECT student.name, student.nationalid, student.programme, COALESCE(undreadmandatory.branch, 'No branch') AS branch , COALESCE(undreadmandatory.coursecode, 'All passed') AS unreadmandatorycourse from student left outer join undreadmandatory on student.nationalid = undreadmandatory.nationalid;

CREATE VIEW recommendedhelper AS SELECT sum(credit) AS numberofrecommendedpassed,nationalid FROM passedcourses NATURAL JOIN recommends GROUP BY nationalid;

CREATE VIEW PathGraduationHelper AS
(SELECT 
    student.nationalid, student.name, student.programme, 
    coalesce(totalcredits.sum, 0) AS totalcredits, 
    coalesce(unpassed.count, 0) AS numberofunpassed, 
    coalesce(research.sum, 0) AS researchcredits, 
    coalesce(mathematical.sum, 0) AS mathcredits, 
    coalesce(seminar.count, 0) AS numberofseminarspassed 
    FROM student 
    LEFT OUTER JOIN 
        (SELECT sum(credit),nationalid FROM passedcourses GROUP BY nationalid) AS totalcredits ON totalcredits.nationalid = student.nationalid 
    LEFT OUTER JOIN (SELECT count(coursecode), nationalid FROM unreadmandatory GROUP BY nationalid) AS unpassed ON unpassed.nationalid = student.nationalid 
    LEFT OUTER JOIN (SELECT sum(credit),nationalid FROM passedcourses 
        NATURAL JOIN has WHERE type = 'Research' GROUP BY nationalid) AS research ON research.nationalid = student.nationalid 
    LEFT OUTER JOIN (SELECT sum(credit), nationalid FROM passedcourses 
        NATURAL JOIN has WHERE type = 'Mathematical' GROUP BY nationalid) AS mathematical ON mathematical.nationalid = student.nationalid 
    LEFT OUTER JOIN (SELECT count(code), nationalid FROM passedcourses 
        NATURAL JOIN has WHERE type = 'Seminar' GROUP BY nationalid) AS seminar ON seminar.nationalid = student.nationalid);

       
CREATE VIEW PathToGraduation AS       
SELECT 
    pathgraduationhelper.nationalid, 
    name, 
    programme, 
    totalcredits, 
    numberofunpassed, 
    researchcredits, 
    mathcredits, 
    numberofseminarspassed, 
    CASE WHEN pathgraduationhelper.totalcredits > 0 
    AND pathgraduationhelper.numberofunpassed = 0 
    AND recommendedhelper.numberofrecommendedpassed >= 10 
    AND pathgraduationhelper.researchcredits >= 10 
    AND pathgraduationhelper.mathcredits >= 20 
    AND pathgraduationhelper.numberofseminarspassed > 0 
    THEN 'Yes' 
    ELSE 'Nope' 
    END AS cangraduate FROM pathgraduationhelper, recommendedhelper;
    
CREATE VIEW CourseQueuePosition AS SELECT * FROM waitsfor;
