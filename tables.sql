--Username: tda_357_043
--Pass: LnWVkGtm
-- \i 'DIRECTORY' 
-- Mass load ^ 

CREATE TABLE Programme(name TEXT, 
		       abbreviation TEXT NOT NULL,
                       PRIMARY KEY(name));

CREATE TABLE Department(name TEXT, 
			abbreviation TEXT NOT NULL, 
			PRIMARY KEY(name), 
			UNIQUE(abbreviation));

CREATE TABLE Hosts(programme TEXT NOT NULL, 
		   department TEXT NOT NULL, 
		   FOREIGN KEY(programme) REFERENCES Programme(name) 
			ON DELETE CASCADE
                        ON UPDATE CASCADE, 
		   FOREIGN KEY(department) REFERENCES Department(name) 
			ON DELETE CASCADE 
			ON UPDATE CASCADE);


CREATE TABLE Student (nationalId INT, 
		      name TEXT NOT NULL, 
                      programme TEXT NOT NULL, 
                      PRIMARY KEY (nationalId), 
		      FOREIGN KEY (programme) REFERENCES programme(name) 
			ON DELETE CASCADE 
			ON UPDATE CASCADE, 
		      UNIQUE(nationalId, programme));

CREATE TABLE Course(name TEXT NOT NULL, 
		    code CHAR(6), 
		    credit INT NOT NULL CHECK (credit > 0),
			department TEXT NOT NULL,
 	            PRIMARY KEY(code),
				FOREIGN KEY(department) REFERENCES department(name));

CREATE TABLE LimitedCourse(code CHAR(6) NOT NULL REFERENCES Course(code) 
				ON DELETE CASCADE
                                ON UPDATE CASCADE, 
                            totalEnrolledAmount INT NOT NULL CHECK (totalEnrolledAmount > 0 ), 
                            PRIMARY KEY (code) );

CREATE TABLE RegisteredTo(nationalId INT NOT NULL, 
			  course CHAR(6) NOT NULL,
			  FOREIGN KEY(nationalId) REFERENCES Student(nationalId) 
				ON DELETE CASCADE
 				ON UPDATE CASCADE,
			  FOREIGN KEY(course) REFERENCES Course(code) 
				ON DELETE CASCADE 
				ON UPDATE CASCADE,
              UNIQUE(nationalid, course));

CREATE TABLE hasFinished(nationalId INT NOT NULL, 
			 code char(6) NOT NULL,
 			 grade CHAR(1) NOT NULL CHECK (grade IN ('U', '3', '4', '5')), 
			 FOREIGN KEY(nationalId) REFERENCES Student(nationalId) 
				ON DELETE CASCADE
				ON UPDATE CASCADE, 
			FOREIGN KEY(code) REFERENCES Course(code) 
				ON DELETE CASCADE 
				ON UPDATE CASCADE,
            UNIQUE(nationalId, code));
            
            
CREATE TABLE Classification (type TEXT NOT NULL, 
			     PRIMARY KEY (type));

CREATE TABLE has(code CHAR(6) NOT NULL, 
    type TEXT NOT NULL, 
    foreign key(code) references course(code) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE, 
    foreign key(type) references classification(type) ON DELETE CASCADE ON UPDATE CASCADE, 
    UNIQUE(code,type));

CREATE TABLE branch(programmeName text NOT NULL, name TEXT NOT NULL, FOREIGN KEY (programmeName) References programme(name) ON DELETE CASCADE ON UPDATE CASCADE, PRIMARY KEY (programmeName, name));


CREATE TABLE reads (nationalid INT NOT NULL, 
		    branch TEXT NOT NULL,  
		    programme TEXT NOT NULL, 
		    FOREIGN KEY(nationalid,programme) REFERENCES Student(nationalid, programme)  
			ON DELETE CASCADE 
			ON UPDATE CASCADE,
		    FOREIGN KEY(branch, programme) REFERENCES branch(name,programmeName)  
			ON DELETE CASCADE 
			ON UPDATE CASCADE,
		     PRIMARY KEY (nationalId)
		   );


CREATE TABLE requires (courseCode CHAR(6) NOT NULL, 
		       reqCourse char(6) NOT NULL, 
			FOREIGN KEY (courseCode) REFERENCES course(code) 
				ON DELETE CASCADE 
				ON UPDATE CASCADE, 
			FOREIGN KEY (reqCourse) REFERENCES Course(code) 
				ON DELETE CASCADE 
				ON UPDATE CASCADE);

CREATE TABLE mandatory(
	programmename TEXT NOT NULL,
	coursecode CHAR (6) NOT NULL,
	FOREIGN KEY (programmename) REFERENCES Programme(name)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	FOREIGN KEY (coursecode) REFERENCES Course(code)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	UNIQUE(programmename, coursecode));

CREATE TABLE demands(programmename TEXT NOT NULL, 
	branchname text not null, 
	cOuRsEcOdE CHAR(6) NOT NULL, 
	foreign key(programmename, branchname) REFERENCES branch(programmename, name)  
		ON DELETE CASCADE 
		ON UPDATE CASCADE, 
	foreign key (cOuRsEcOdE) referenceS course(code) 
		ON DELETE CASCADE 
		ON UPDATE CASCADE);

--CREATE TABLE gives (department text NOt NUll, courseCode CHAR(6) NOT NULL, FOREIGN KEY (courseCode) REFERENCES course(code) ON DELETE CASCADE ON UPDATE CASCADE, FOREIGN KEY (department) REFERENCES department(name) ON DELETE CASCADE ON UPDATE CASCADE);

create table holds  (name text not null, code char(6) NOT NULL, foreign key(name) references programme(name) ON DELETE CASCADE
                               ON UPDATE CASCADE, foreign key (code) references course(code) ON DELETE CASCADE
                               ON UPDATE CASCADE);


create table recommends(code char(6) NOT NULL,programmename TEXT NOT NULL, branchname text not null, foreign key(programmename, branchname) REFERENCES branch(programmename, name) ON DELETE CASCADE ON UPDATE CASCADE, foreign key (cOdE) referenceS course(code) ON DELETE CASCADE ON UPDATE CASCADE);

--*******************************************
-- ADDED UNIQUE (nationalid, code) HERE 2016-12-11
--*******************************************

create table waitsFor( nationalId int NOT NULL, code char(6) NOT NULL, queuePosition int NOT NULL check (queuePosition > 0), 
                            Foreign Key (nationalId) references student(nationalId)
                            ON DELETE CASCADE
                               ON UPDATE CASCADE,
                               FOREIGN KEY (code) REFERENCES limitedcourse(code)
                               ON DELETE CASCADE
                               ON UPDATE CASCADE,
                               UNIQUE (code, queuePosition),
                               UNIQUE (nationalid, code)
                               );


 SELECT * FROM BRANCH JOIN PROGRAMME ON branch.programmename = programme.name;



