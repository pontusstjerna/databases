-- 950000 , NULL
-- 950630 , NULL
-- 950000 , POL51
-- 950000 , POL51
-- 950000 , POL51
-- 950000 , POL51
-- 950630 , MIT620
-- 950000 , POL53
-- 950000 , POL53
-- 950000 , POL53


--INSERT INTO hasFinished VALUES (950630,'DIT666','5');
INSERT INTO hasFinished VALUES (950630,'DIT512','4');
INSERT INTO hasFinished VALUES (950630,'DIT123','3');

INSERT INTO course VALUES('MegaDatabases', 'MIT620', 7, 'Computer Science and Engineering');
INSERT INTO course VALUES('MegaAlgorithms', 'MIT010', 7, 'Computer Science and Engineering');
INSERT INTO requires VALUES('MIT620', 'MIT010');
--INSERT INTO registeredTo VALUES(950630, 'MIT620');
INSERT INTO registeredto VALUES (950630,'POL53');
INSERT INTO registeredto VALUES (5,'POL53');
--DELETE FROM registeredTo WHERE (nationalid, course) = (950000, 'POL53');

ALTER TABLE registeredTo DISABLE TRIGGER USER;
INSERT INTO registeredto VALUES (0, 'POL53');
ALTER TABLE registeredTo ENABLE TRIGGER USER;
