--Username: tda_357_043
--Pass: LnWVkGtm
-- \i 'DIRECTORY' 
-- Mass load ^ 

CREATE TABLE Programme(name TEXT, 
		       abbreviation TEXT NOT NULL,
                       PRIMARY KEY(name));

CREATE TABLE Department(name TEXT, 
			abbreviation TEXT NOT NULL, 
			PRIMARY KEY(name), 
			UNIQUE(abbreviation));

CREATE TABLE Hosts(programme TEXT NOT NULL, 
		   department TEXT NOT NULL, 
		   FOREIGN KEY(programme) REFERENCES Programme(name) 
			ON DELETE CASCADE
                        ON UPDATE CASCADE, 
		   FOREIGN KEY(department) REFERENCES Department(name) 
			ON DELETE CASCADE 
			ON UPDATE CASCADE);


CREATE TABLE Student (nationalId INT, 
		      name TEXT NOT NULL, 
                      programme TEXT NOT NULL, 
                      PRIMARY KEY (nationalId), 
		      FOREIGN KEY (programme) REFERENCES programme(name) 
			ON DELETE CASCADE 
			ON UPDATE CASCADE, 
		      UNIQUE(nationalId, programme));

CREATE TABLE Course(name TEXT NOT NULL, 
		    code CHAR(6), 
		    credit INT NOT NULL CHECK (credit > 0),
			department TEXT NOT NULL,
 	            PRIMARY KEY(code),
				FOREIGN KEY(department) REFERENCES department(name));

CREATE TABLE LimitedCourse(code CHAR(6) NOT NULL REFERENCES Course(code) 
				ON DELETE CASCADE
                                ON UPDATE CASCADE, 
                            totalEnrolledAmount INT NOT NULL CHECK (totalEnrolledAmount > 0 ), 
                            PRIMARY KEY (code) );

CREATE TABLE RegisteredTo(nationalId INT NOT NULL, 
			  course CHAR(6) NOT NULL,
			  FOREIGN KEY(nationalId) REFERENCES Student(nationalId) 
				ON DELETE CASCADE
 				ON UPDATE CASCADE,
			  FOREIGN KEY(course) REFERENCES Course(code) 
				ON DELETE CASCADE 
				ON UPDATE CASCADE,
              UNIQUE(nationalid, course));

CREATE TABLE hasFinished(nationalId INT NOT NULL, 
			 code char(6) NOT NULL,
 			 grade CHAR(1) NOT NULL CHECK (grade IN ('U', '3', '4', '5')), 
			 FOREIGN KEY(nationalId) REFERENCES Student(nationalId) 
				ON DELETE CASCADE
				ON UPDATE CASCADE, 
			FOREIGN KEY(code) REFERENCES Course(code) 
				ON DELETE CASCADE 
				ON UPDATE CASCADE,
            UNIQUE(nationalId, code));
            
            
CREATE TABLE Classification (type TEXT NOT NULL, 
			     PRIMARY KEY (type));

CREATE TABLE has(code CHAR(6) NOT NULL, 
    type TEXT NOT NULL, 
    foreign key(code) references course(code) 
    ON DELETE CASCADE 
    ON UPDATE CASCADE, 
    foreign key(type) references classification(type) ON DELETE CASCADE ON UPDATE CASCADE, 
    UNIQUE(code,type));

CREATE TABLE branch(programmeName text NOT NULL, name TEXT NOT NULL, FOREIGN KEY (programmeName) References programme(name) ON DELETE CASCADE ON UPDATE CASCADE, PRIMARY KEY (programmeName, name));


CREATE TABLE reads (nationalid INT NOT NULL, 
		    branch TEXT NOT NULL,  
		    programme TEXT NOT NULL, 
		    FOREIGN KEY(nationalid,programme) REFERENCES Student(nationalid, programme)  
			ON DELETE CASCADE 
			ON UPDATE CASCADE,
		    FOREIGN KEY(branch, programme) REFERENCES branch(name,programmeName)  
			ON DELETE CASCADE 
			ON UPDATE CASCADE,
		     PRIMARY KEY (nationalId)
		   );


CREATE TABLE requires (courseCode CHAR(6) NOT NULL, 
		       reqCourse char(6) NOT NULL, 
			FOREIGN KEY (courseCode) REFERENCES course(code) 
				ON DELETE CASCADE 
				ON UPDATE CASCADE, 
			FOREIGN KEY (reqCourse) REFERENCES Course(code) 
				ON DELETE CASCADE 
				ON UPDATE CASCADE);

CREATE TABLE mandatory(
	programmename TEXT NOT NULL,
	coursecode CHAR (6) NOT NULL,
	FOREIGN KEY (programmename) REFERENCES Programme(name)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	FOREIGN KEY (coursecode) REFERENCES Course(code)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	UNIQUE(programmename, coursecode));

CREATE TABLE demands(programmename TEXT NOT NULL, 
	branchname text not null, 
	cOuRsEcOdE CHAR(6) NOT NULL, 
	foreign key(programmename, branchname) REFERENCES branch(programmename, name)  
		ON DELETE CASCADE 
		ON UPDATE CASCADE, 
	foreign key (cOuRsEcOdE) referenceS course(code) 
		ON DELETE CASCADE 
		ON UPDATE CASCADE);

--CREATE TABLE gives (department text NOt NUll, courseCode CHAR(6) NOT NULL, FOREIGN KEY (courseCode) REFERENCES course(code) ON DELETE CASCADE ON UPDATE CASCADE, FOREIGN KEY (department) REFERENCES department(name) ON DELETE CASCADE ON UPDATE CASCADE);

create table holds  (name text not null, code char(6) NOT NULL, foreign key(name) references programme(name) ON DELETE CASCADE
                               ON UPDATE CASCADE, foreign key (code) references course(code) ON DELETE CASCADE
                               ON UPDATE CASCADE);


create table recommends(code char(6) NOT NULL,programmename TEXT NOT NULL, branchname text not null, foreign key(programmename, branchname) REFERENCES branch(programmename, name) ON DELETE CASCADE ON UPDATE CASCADE, foreign key (cOdE) referenceS course(code) ON DELETE CASCADE ON UPDATE CASCADE);

--*******************************************
-- ADDED UNIQUE (nationalid, code) HERE 2016-12-11
--*******************************************

create table waitsFor( nationalId int NOT NULL, code char(6) NOT NULL, queuePosition int NOT NULL check (queuePosition > 0), 
                            Foreign Key (nationalId) references student(nationalId)
                            ON DELETE CASCADE
                               ON UPDATE CASCADE,
                               FOREIGN KEY (code) REFERENCES limitedcourse(code)
                               ON DELETE CASCADE
                               ON UPDATE CASCADE,
                               UNIQUE (code, queuePosition),
                               UNIQUE (nationalid, code)
                               );


 SELECT * FROM BRANCH JOIN PROGRAMME ON branch.programmename = programme.name;

-- ***********************************************
-- INSERTIONS
-- ***********************************************

INSERT INTO department VALUES('Computer Science and Engineering', 'CSE');
INSERT INTO department VALUES('Nature Science and Abusement', 'NSA');

INSERT INTO programme VALUES('Computer Science', 'CS');
INSERT INTO programme VALUES('Fishing', 'FSK');
INSERT INTO programme VALUES('Hunting', 'HNT');

INSERT INTO hosts VALUES('Computer Science', 'Computer Science and Engineering');
INSERT INTO hosts VALUES('Fishing', 'Nature Science and Abusement');
INSERT INTO hosts VALUES('Hunting', 'Nature Science and Abusement');


INSERT INTO course VALUES('Databases', 'DIT620', 7, 'Computer Science and Engineering');
INSERT INTO course VALUES('Algorithms', 'DIT010', 7, 'Computer Science and Engineering');
INSERT INTO course VALUES('Coding', 'DIT555', 7, 'Computer Science and Engineering');
INSERT INTO course VALUES('Computer Rooms', 'DIT512', 7, 'Computer Science and Engineering');
INSERT INTO course VALUES('Infinite Automata', 'DIT123', 8, 'Computer Science and Engineering');
INSERT INTO course VALUES('Anger Management', 'DIT666', 666, 'Computer Science and Engineering');
INSERT INTO course VALUES('Bait Application', 'BAI136', 1, 'Nature Science and Abusement');
INSERT INTO course VALUES('Fish Simulation', 'BAI344', 1, 'Nature Science and Abusement');
INSERT INTO course VALUES('Arctic Fishing', 'BAI500', 1, 'Nature Science and Abusement');
INSERT INTO course VALUES('Forest Running', 'FOR500', 2, 'Nature Science and Abusement');
INSERT INTO course VALUES('Tree hugging', 'FOR501', 2, 'Nature Science and Abusement');
INSERT INTO course VALUES('Deer Anatomy', 'HUN120', 3, 'Nature Science and Abusement');
INSERT INTO course VALUES('Rabbit Slaughter', 'HUN121', 3, 'Nature Science and Abusement');
INSERT INTO course VALUES('Rifle Handling', 'HUN122', 3, 'Nature Science and Abusement');
INSERT INTO course VALUES('Fletching', 'HUN123', 3, 'Nature Science and Abusement');
INSERT INTO course VALUES('Trumping', 'POL53', 4, 'Computer Science and Engineering');
INSERT INTO course VALUES('Not Trumping', 'POL54', 4, 'Computer Science and Engineering');
INSERT INTO course VALUES('Analytics', 'POL51', 4, 'Computer Science and Engineering');

INSERT INTO mandatory VALUES('Computer Science', 'DIT629');
INSERT INTO mandatory VALUES('Computer Science', 'DIT010');
INSERT INTO mandatory VALUES('Computer Science', 'DIT555');
INSERT INTO mandatory VALUES('Computer Science', 'DIT512');
INSERT INTO mandatory VALUES('Computer Science', 'DIT123');
INSERT INTO mandatory VALUES('Computer Science', 'DIT666');
INSERT INTO mandatory VALUES('Fishing', 'BAI136');
INSERT INTO mandatory VALUES('Fishing', 'BAI344');
INSERT INTO mandatory VALUES('Fishing', 'BAI500');
INSERT INTO mandatory VALUES('Hunting', 'HUN120');
INSERT INTO mandatory VALUES('Hunting', 'HUN121');
INSERT INTO mandatory VALUES('Hunting', 'HUN122');
INSERT INTO mandatory VALUES('Hunting', 'HUN123');


INSERT INTO limitedcourse VALUES('POL53', 2);
INSERT INTO limitedcourse VALUES('POL54', 1);
INSERT INTO limitedcourse VALUES('BAI344', 50);

INSERT INTO holds VALUES('Fishing', 'BAI136');
INSERT INTO holds VALUES('Fishing', 'BAI344');
INSERT INTO holds VALUES('Fishing', 'BAI500');
INSERT INTO holds VALUES('Computer Science', 'DIT620');
INSERT INTO holds VALUES('Computer Science', 'DIT010');
INSERT INTO holds VALUES('Computer Science', 'DIT555');
INSERT INTO holds VALUES('Computer Science', 'DIT512');
INSERT INTO holds VALUES('Computer Science', 'DIT123');
INSERT INTO holds VALUES('Computer Science', 'DIT666');
INSERT INTO holds VALUES('Hunting', 'HUN120');
INSERT INTO holds VALUES('Hunting', 'HUN121');
INSERT INTO holds VALUES('Hunting', 'HUN122');
INSERT INTO holds VALUES('Hunting', 'HUN123');

INSERT INTO student VALUES (5, 'Magnus Efternamn', 'Computer Science');
INSERT INTO student VALUES (150, 'Clark Kent', 'Computer Science');
INSERT INTO student VALUES (950630, 'Pontus Stjernstrom', 'Computer Science');
INSERT INTO student VALUES (950000, 'Aleksandar Babunovic', 'Hunting');
INSERT INTO student VALUES (151, 'Bruce  Willis', 'Hunting');
INSERT INTO student VALUES (0, 'Nat Pagle', 'Fishing');
INSERT INTO student VALUES (18001, 'Fredik Reinfeldt', 'Fishing');
INSERT INTO student VALUES (1, 'Hemet Nesingwary', 'Hunting');

INSERT INTO branch VALUES ('Hunting', 'Godzilla Hunting');
INSERT INTO branch VALUES ('Fishing', 'Sushi Fishing');
INSERT INTO branch VALUES ('Computer Science', 'AI');
INSERT INTO branch VALUES ('Computer Science', 'Security');
INSERT INTO branch VALUES ('Hunting', 'Simulation');

INSERT INTO classification VALUES ('Mathematical');
INSERT INTO classification VALUES ('Research');
INSERT INTO classification VALUES ('Seminar');

INSERT INTO registeredTo VALUES (0, 'BAI344');
INSERT INTO registeredTo VALUES (1, 'HUN123');
INSERT INTO registeredTo VALUES (1, 'HUN122');
INSERT INTO registeredTo VALUES (1, 'HUN121');
INSERT INTO registeredTo VALUES (18001, 'POL53');
INSERT INTO registeredTo VALUES (950630, 'DIT666');
INSERT INTO registeredto VALUES('950000', 'POL53');

INSERT INTO hasFinished VALUES (0, 'BAI500','5');
INSERT INTO hasFinished VALUES (0, 'BAI136', '5');
INSERT INTO hasFinished VALUES (950000, 'HUN121','U');
INSERT INTO hasfinished values (950630, 'DIT555', '3');
INSERT INTO hasfinished values (950630, 'DIT666', '5');
INSERT INTO hasfinished values (950630, 'DIT010', '4');
INSERT INTO hasfinished values (5, 'DIT010', '4');

INSERT INTO demands VALUES ('Hunting', 'Godzilla Hunting', 'HUN121');
INSERT INTO demands VALUES ('Fishing', 'Sushi Fishing', 'BAI136');
INSERT INTO demands VALUES ('Computer Science', 'Security', 'POL54');

INSERT INTO recommends VALUES ('DIT123', 'Fishing', 'Sushi Fishing');
INSERT INTO recommends VALUES ('DIT555', 'Hunting', 'Godzilla Hunting');

INSERT INTO reads VALUES (950000, 'Godzilla Hunting', 'Hunting');
INSERT INTO reads VALUES (5, 'Security', 'Computer Science');

INSERT INTO has VALUES ('DIT555', 'Mathematical');
INSERT INTO has VALUES ('DIT010', 'Mathematical');
INSERT INTO has VALUES ('DIT666', 'Mathematical');
INSERT INTO has VALUES ('DIT666', 'Research');
INSERT INTO has VALUES ('DIT123', 'Research');
INSERT INTO has VALUES ('DIT555', 'Seminar');

-- **********************************
-- VIEWS
-- **********************************


CREATE VIEW StudentsFollowing 
	AS SELECT student.nationalid, student.name, student.programme, reads.branch 
    FROM student 
    LEFT OUTER JOIN reads 
        ON student.nationalid = reads.nationalid;

CREATE VIEW FinishedCourses 
	AS SELECT *
	FROM hasFinished 
	NATURAL JOIN course 
	NATURAL JOIN 
		(SELECT nationalId, name AS studentName, programme from student) AS temp;
        
CREATE VIEW PassedCourses 
    AS SELECT * 
    FROM hasfinished 
    NATURAL JOIN course 
    NATURAL JOIN 
        (SELECT nationalid, name AS studentName, programme FROM student) AS temp 
    WHERE grade != 'U';

CREATE VIEW UnreadMandatory
AS 
(select nationalId, coursecode from reads   join demands   ON reads.branch = demands.branchname WHERE (demands.coursecode, reads.nationalid)  NOT IN (select code, nationalid from passedcourses))
UNION
(select nationalId, coursecode from student join mandatory ON student.programme = mandatory.programmename WHERE (mandatory.coursecode, student.nationalid)  NOT IN (select code, nationalid from passedcourses)) ORDER BY nationalid;


CREATE VIEW Registrations AS
(SELECT student.nationalid, student.name AS StudentName, course.code AS CourseCode, course.name AS CourseName, 'Waiting' AS Status 
FROM waitsfor, student, course 
WHERE student.nationalid = waitsfor.nationalid 
AND waitsfor.code = course.code) UNION (SELECT student.nationalid AS StudentId, student.name AS StudentName, course.code AS CourseCode, course.name AS CourseName, 'Registered' AS Status 
FROM registeredto, student, course WHERE student.nationalid = registeredto.nationalid AND registeredto.course = course.code);

--SELECT student.name, student.nationalid, student.programme, COALESCE(undreadmandatory.branch, 'No branch') AS branch , COALESCE(undreadmandatory.coursecode, 'All passed') AS unreadmandatorycourse from student left outer join undreadmandatory on student.nationalid = undreadmandatory.nationalid;

CREATE VIEW recommendedhelper AS SELECT sum(credit) AS numberofrecommendedpassed,nationalid FROM passedcourses NATURAL JOIN recommends GROUP BY nationalid;

CREATE VIEW PathGraduationHelper AS
(SELECT 
    student.nationalid, student.name, student.programme, 
    coalesce(totalcredits.sum, 0) AS totalcredits, 
    coalesce(unpassed.count, 0) AS numberofunpassed, 
    coalesce(research.sum, 0) AS researchcredits, 
    coalesce(mathematical.sum, 0) AS mathcredits, 
    coalesce(seminar.count, 0) AS numberofseminarspassed 
    FROM student 
    LEFT OUTER JOIN 
        (SELECT sum(credit),nationalid FROM passedcourses GROUP BY nationalid) AS totalcredits ON totalcredits.nationalid = student.nationalid 
    LEFT OUTER JOIN (SELECT count(coursecode), nationalid FROM unreadmandatory GROUP BY nationalid) AS unpassed ON unpassed.nationalid = student.nationalid 
    LEFT OUTER JOIN (SELECT sum(credit),nationalid FROM passedcourses 
        NATURAL JOIN has WHERE type = 'Research' GROUP BY nationalid) AS research ON research.nationalid = student.nationalid 
    LEFT OUTER JOIN (SELECT sum(credit), nationalid FROM passedcourses 
        NATURAL JOIN has WHERE type = 'Mathematical' GROUP BY nationalid) AS mathematical ON mathematical.nationalid = student.nationalid 
    LEFT OUTER JOIN (SELECT count(code), nationalid FROM passedcourses 
        NATURAL JOIN has WHERE type = 'Seminar' GROUP BY nationalid) AS seminar ON seminar.nationalid = student.nationalid);

       
CREATE VIEW PathToGraduation AS       
SELECT 
    pathgraduationhelper.nationalid, 
    name, 
    programme, 
    totalcredits, 
    numberofunpassed, 
    researchcredits, 
    mathcredits, 
    numberofseminarspassed, 
    CASE WHEN pathgraduationhelper.totalcredits > 0 
    AND pathgraduationhelper.numberofunpassed = 0 
    AND recommendedhelper.numberofrecommendedpassed >= 10 
    AND pathgraduationhelper.researchcredits >= 10 
    AND pathgraduationhelper.mathcredits >= 20 
    AND pathgraduationhelper.numberofseminarspassed > 0 
    THEN 'Yes' 
    ELSE 'Nope' 
    END AS cangraduate FROM pathgraduationhelper, recommendedhelper;
    
CREATE VIEW CourseQueuePosition AS SELECT * FROM waitsfor;


